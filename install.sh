#!/bin/bash
echo "Do you wish to install this program?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) yum install -y epel-release &&
              yum install -y \
              mariadb-server\
              httpd\
              php-xml\
              php-pdo\
              php-mysql\
              php-cli\
              php\
              php-gd\
              php-soap\
              php-mbstring\
              redis\
              vim\
              git\
              npm\
              ruby\
              wget;
              systemctl enable httpd mariadb redis;
             break;;
        No ) break;;
    esac
done

echo "Install Composer?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) curl -sS https://getcomposer.org/installer | php;
              mv composer.phar /usr/local/bin/composer;
             break;;
        No ) break;;
    esac
done

echo "Install Gulp and Bower?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) npm install --global gulp;
              npm install --global bower;
             break;;
        No ) break;;
    esac
done

echo "Install SASS?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) gem install sass;
             break;;
        No ) break;;
    esac
done

echo "Enable httpd_anon_write?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) setsebool -P httpd_anon_write on;
              setsebool httpd_anon_write on;
             break;;
        No ) break;;
    esac
done

echo "Enable httpd_can_network_connect?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) setsebool -P httpd_can_network_connect on;
              setsebool httpd_can_network_connect on;
             break;;
        No ) break;;
    esac
done

exit;
